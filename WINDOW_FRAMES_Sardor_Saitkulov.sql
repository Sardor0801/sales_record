SELECT time_id, day_name, calendar_week_number, sales_amount,
    SUM(sales_amount) OVER (PARTITION BY calendar_week_number ORDER BY time_id RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS cum_sum,
	    CASE
        WHEN day_number_in_week = 1 THEN
            ROUND(AVG(sales_amount) OVER (ORDER BY time_id RANGE BETWEEN INTERVAL '2' DAY PRECEDING AND INTERVAL '1' DAY FOLLOWING), 2)
        WHEN day_number_in_week = 5 THEN
            ROUND(AVG(sales_amount) OVER (ORDER BY time_id RANGE BETWEEN INTERVAL '1' DAY PRECEDING AND INTERVAL '2' DAY FOLLOWING), 2)
        ELSE
            ROUND(AVG(sales_amount) OVER (ORDER BY time_id RANGE BETWEEN INTERVAL '1' DAY PRECEDING AND INTERVAL '1' DAY FOLLOWING), 2)
    END AS centered_3_day_avg
FROM (
    SELECT t.time_id, t.day_number_in_week, DATE_TRUNC('week', t.time_id) AS calendar_week_number, t.day_name, SUM(amount_sold) AS sales_amount
    FROM sh.sales
	JOIN sh.times t USING(time_id)
    WHERE DATE_TRUNC('week', t.time_id) IN ('1999-49', '1999-50', '1999-51')
    GROUP BY t.time_id, calendar_week_number, t.day_name, t.day_number_in_week
	ORDER BY time_id
) AS subquery;